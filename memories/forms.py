from django import forms
from django.forms.formsets import BaseFormSet
from django.forms import TextInput, Textarea, CheckboxInput
from .models import Post, Picture, Comment
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from dateutil.parser import parse


class TextForm(forms.ModelForm):
    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='button is-primary'))
    helper.form_method = 'POST'

    class Meta:
        model = Post
        fields = ('text', 'timeline_date',)
        widgets = {
            'text': Textarea(attrs={'class': "textarea", 'placeholder': 'Write your text here', 'cols': '', 'rows': '3'}),
        }


class CommentForm(forms.ModelForm):
    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='button is-primary'))
    helper.form_method = 'POST'

    class Meta:
        model = Comment
        fields = ('text',)
        labels = {"text": "Comment"}


class FileFieldForm(forms.Form):
    helper = FormHelper()
    helper.add_input(Submit('submit', 'Submit', css_class='button is-primary'))
    helper.form_method = 'POST'
    images_field = forms.ImageField(widget=forms.ClearableFileInput(attrs={'multiple': True}))


class PictureForm(forms.Form):
    text = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': "textarea", 'placeholder': 'Write your text here', 'cols': '', 'rows': '3'}))
    date_taken = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': "input", 'placeholder': 'date_taken'}))
    location = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': "input", 'placeholder': 'location'}))
    delete = forms.BooleanField(required=False, widget=forms.CheckboxInput())

    # class Meta:
    #     fields = ('text', 'location', 'date_taken', 'delete')
    #     widgets = {
    #         'text': TextInput(attrs={'class': "input", 'placeholder': 'Caption'}),
    #         'date_taken': TextInput(attrs={'class': "input", 'placeholder': 'Caption'}),
    #         'location': TextInput(attrs={'class': "input", 'placeholder': 'Location'}),
    #         'delete': CheckboxInput(attrs={'class': "input", 'placeholder': 'delete'}),
    #     }


class PostForm(forms.Form):
    text = forms.CharField(required=False, widget=forms.Textarea(attrs={'class': "textarea", 'placeholder': 'Write your text here', 'cols': '', 'rows': '3'}))
    timeline_date = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': "input", 'placeholder': 'timeline_date'}))
    post_as_one_unit = forms.BooleanField(required=False, widget=forms.CheckboxInput())

    def clean(self):
        """Form validation"""
        cleaned_data = super().clean()
        text = cleaned_data.get("text")
        if text == "some string":
            self.add_error('location', "Incorrect input")
            raise forms.ValidationError("The total sum is not zero! Which means that something is not right.")
        date = cleaned_data['timeline_date']
        if date == "" or date == " ":
            cleaned_data['timeline_date'] = None
        else:
            if not is_date(date):
                self.add_error('timeline_date', "Incorrect date format")
                raise forms.ValidationError("Couldn't parse date string, try a different format")


def is_date(string, fuzzy=False):
    """
    Return whether the string can be interpreted as a date.

    :param string: str, string to check for date
    :param fuzzy: bool, ignore unknown tokens in string if True
    """
    try:
        parse(string, fuzzy=fuzzy)
        return True

    except ValueError:
        return False


class BasePictureFormSet(BaseFormSet):
    def clean(self):
        """Form validation"""
        if any(self.errors):  # Don't bother validating the formset unless each form is valid on its own
            return
        for form in self.forms:  # TODO: This validation is literally nonsense.. remember to make something real
            location = form.cleaned_data.get("location")
            if location == "some string":
                form.add_error('location', "Incorrect input")
                raise forms.ValidationError("The total sum is not zero! Which means that something is not right.")
            caption = form.cleaned_data.get("caption")
            if caption == "some string":
                form.add_error('caption', "Incorrect input")
                raise forms.ValidationError("The total sum is not zero! Which means that something is not right.")
            date = form.cleaned_data['date_taken']
            if date == "" or date == " ":
                form.cleaned_data['date_taken'] = None
            else:
                if not is_date(date):
                    form.add_error('date_taken', "Incorrect date format")
                    raise forms.ValidationError("Couldn't parse date string, try a different format")
