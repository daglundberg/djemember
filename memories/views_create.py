from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.forms import formset_factory
from django.utils import timezone
from .models import Post, Picture
from .forms import TextForm, CommentForm, PictureForm, BasePictureFormSet, PostForm
import datetime
from dateutil.parser import parse


@login_required
def comment_create(request, post_id):
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.user = request.user
            comment.is_comment_on = Post.objects.get(pk=post_id)
            comment.pub_date = timezone.now()
            comment.save()
            return redirect('timeline')
    else:
        form = CommentForm()
        post = Post.objects.get(pk=post_id)
    return render(request, 'memories/comment_create.html', {'form': form, 'post': post})


@login_required
def post_create(request):
    unpublished_pictures = Picture.objects.filter(user=request.user).filter(timeline_date__isnull=True).filter(is_in_post__isnull=True)

    # Create a python list with query result so we can remove items from it easier (in case one is marked for deletion in the formset)
    upl = list()
    for up in unpublished_pictures:
        upl.append(up)

    # Create a list with the picture urls to send to the form template
    pic_urls = list()
    for pic in upl:
        pic_urls.append(pic.image.url)
    PictureFormSet = formset_factory(PictureForm, formset=BasePictureFormSet, extra=0, min_num=0, validate_min=True)

    if request.method == 'POST':
        data = request.POST or None
        formset = PictureFormSet(data=data)
        form = PostForm(request.POST)
        if all([form.is_valid(), formset.is_valid()]):
            number_of_forms = len(formset)

            # First of all delete all picture forms marked for deletion
            for inline_form, pic in zip(formset, upl):
                if inline_form.cleaned_data:
                    if inline_form.cleaned_data['delete']:
                        upl.remove(pic)
                        number_of_forms -= 1
                        pic.delete()

            # Then we check if there are no pictures posted
            if number_of_forms == 0:
                # no pictures posted, this is a text only post.
                # TODO: Seems like we get here without going through form validation??
                save_post(request.user, form.cleaned_data['text'], form.cleaned_data['timeline_date'])
                messages.success(request, 'Saved text only post.')
                return redirect('timeline')

            elif number_of_forms > 0:
                # Then we check if this is posted as one unit or as individual posts
                if form.cleaned_data['post_as_one_unit'] is True:

                    post = save_post(request.user, form.cleaned_data['text'], form.cleaned_data['timeline_date'])
                    for inline_form, pic in zip(formset, upl):
                        if inline_form.cleaned_data:
                            save_pic_to_post(pic, inline_form.cleaned_data['date_taken'], inline_form.cleaned_data['text'], inline_form.cleaned_data['location'], post)

                    messages.success(request, 'Saved muliple pictures as one unit.')
                    return redirect('timeline')

                else:
                    # We are posting indivdual posts to timeline
                    if form.cleaned_data['text'] != "":
                        save_post(request.user, form.cleaned_data['text'], form.cleaned_data['timeline_date'])

                    for inline_form, pic in zip(formset, upl):
                        save_pic_to_timeline(pic, inline_form.cleaned_data['date_taken'], inline_form.cleaned_data['text'], inline_form.cleaned_data['location'])

                    messages.success(request, 'Saved muliple pictures as individual posts.')
                    return redirect('timeline')

    else:
        formset = PictureFormSet(initial=unpublished_pictures.values())
        form = PostForm()
    return render(request, 'memories/all_create.html', {'form': form, 'formset': formset, 'pic_urls': pic_urls})


def save_post(user, text, timeline_date):
    post = Post(timeline_date=timeline_date, user=user, text=text)
    if timeline_date is None:
        post.timeline_date = datetime.datetime.now()
    post.save()
    return post


def save_pic_to_timeline(picture, date_taken, text, location):
    pic = picture
    pic.text = text
    if date_taken is None:
        pic.date_taken = datetime.datetime.now()
    else:
        pic.date_taken = parse(date_taken, fuzzy=False)
    pic.timeline_date = pic.date_taken
    pic.location = location
    pic.save()


def save_pic_to_post(picture, date_taken, text, location, post):
    pic = picture
    pic.text = text
    if date_taken is None:
        pic.date_taken = datetime.datetime.now()
    else:
        pic.date_taken = parse(date_taken, fuzzy=False)
    pic.location = location
    pic.is_in_post = post
    pic.save()
